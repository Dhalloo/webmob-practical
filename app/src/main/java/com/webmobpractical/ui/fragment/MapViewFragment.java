package com.webmobpractical.ui.fragment;

import android.Manifest;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.webmobpractical.R;
import com.webmobpractical.database.DBHelper;
import com.webmobpractical.network.instagram.InstagramImages;
import com.webmobpractical.network.instagram.MapLocations;
import com.webmobpractical.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Created by Dhalloo on 11/13/2017.
 */

public class MapViewFragment extends BaseFragment implements OnMapReadyCallback,
        EasyPermissions.PermissionCallbacks, LocationListener {

    private GoogleMap mMap;
    private static final int RC_LOCATION_PERM = 122;
    public ArrayList<InstagramImages.Data> dataArrayList = new ArrayList<>();
    public ArrayList<MapLocations> mapLocations;
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private DBHelper mDbHelper;

    @AfterPermissionGranted(RC_LOCATION_PERM)
    @Override
    protected void init(View mView) {

        mDbHelper = new DBHelper(mActivityContext);

        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (EasyPermissions.hasPermissions(getContext(), perms)) {
            SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
            locationManager = (LocationManager) mActivityContext.getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
        } else {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_location), RC_LOCATION_PERM, perms);
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.fragment_map_view;
    }

    @AfterPermissionGranted(RC_LOCATION_PERM)
    @Override
    public void onMapReady(GoogleMap googleMap) {

        fetchImagesFromDatabase();

        mMap = googleMap;

        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);
    }

    private void fetchImagesFromDatabase() {
        Cursor mCursor = mDbHelper.getValues(DBHelper.TABLE_INSTA_IMAGES, null, null);

        if (mCursor != null
                && mCursor.getCount() != 0) {
            if (mCursor.moveToFirst()) {
                do {
                    String imageId = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_ID));
                    String imageLikes = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LIKES));
                    String imageLocation = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOCATION));
                    String imageLocationName = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOCATION_NAME));
                    String imageLowUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOW_RES_URL));
                    String imageStandardUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_STANDARD_RES_URL));
                    String imageThumbnailUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_THUMBNAIL_URL));

                    InstagramImages mInstagramImages = new InstagramImages();
                    InstagramImages.Data mData = mInstagramImages.new Data();
                    InstagramImages.Data.Likes mLikes = mData.new Likes();
                    InstagramImages.Data.Images mImages = mData.new Images();
                    InstagramImages.Data.Images.Resolutions mLowRes = mImages.new Resolutions();
                    InstagramImages.Data.Images.Resolutions mStdRes = mImages.new Resolutions();
                    InstagramImages.Data.Images.Resolutions mThumbRes = mImages.new Resolutions();
                    InstagramImages.Data.Location mLocation = mData.new Location();

                    mData.setId(imageId);

                    mLocation.setLocation(imageLocation);
                    mLocation.setName(imageLocationName);
                    mData.setLocation(mLocation);

                    mLikes.setCount(Integer.valueOf(imageLikes));
                    mData.setLikes(mLikes);

                    mLowRes.setUrl(imageLowUrl);
                    mStdRes.setUrl(imageStandardUrl);
                    mThumbRes.setUrl(imageThumbnailUrl);
                    mImages.setThumbnail(mThumbRes);
                    mImages.setStandard_resolution(mStdRes);
                    mImages.setLow_resolution(mLowRes);
                    mData.setImages(mImages);

                    dataArrayList.add(mData);

                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        applyMarkers();
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        Utils.print("onPermissionsGranted:" + requestCode + ":" + perms.size());
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        locationManager = (LocationManager) mActivityContext.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Utils.print("onPermissionsDenied:" + requestCode + ":" + perms.size());
    }

    private void applyMarkers() {
        mapLocations = new ArrayList<>();
        for (InstagramImages.Data mData : dataArrayList) {
            if (mData.getLocation() != null
                    && !mData.getLocation().getLocation().equalsIgnoreCase("")) {
                String[] latlong = mData.getLocation().getLocation().split(",");
                mapLocations.add(new MapLocations(Double.parseDouble(latlong[0]),
                        Double.parseDouble(latlong[1]),
                        mData.getLocation().getName(),
                        mData.getImages().getThumbnail().getUrl()));
                Utils.print(mData.getImages().getThumbnail().getUrl());
            }
        }

        for (MapLocations locations : mapLocations) {
            final LatLng position = locations.getmPosition();

            Utils.print("USER IMAGE " + locations.getUserImage());
            final MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(position);
            markerOptions.title(locations.getmTitle());

            Glide.with(mActivityContext)
                    .load(locations.getUserImage())
                    .asBitmap()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(Bitmap bitmap, GlideAnimation<? super Bitmap> glideAnimation) {
                            // let's find marker for this user
                            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bitmap));
                            mMap.addMarker(markerOptions);
                        }
                    });
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 10);
        mMap.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }
}
