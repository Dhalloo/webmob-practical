package com.webmobpractical.ui.fragment;

import android.content.Context;
import android.content.res.Configuration;
import android.database.Cursor;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;

import com.webmobpractical.R;
import com.webmobpractical.database.DBHelper;
import com.webmobpractical.network.instagram.InstagramImages;
import com.webmobpractical.ui.adapter.HomeInstagramImagesAdapter;

import java.util.ArrayList;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class HomeFragment extends BaseFragment {

    public static final String EMPTY_IMAGE_PATH = "image";
    private ArrayList<InstagramImages.Data> dataArrayList = new ArrayList<>();
    private RecyclerView mRecyclerInstaImages;
    private HomeInstagramImagesAdapter mAdapter;
    private DBHelper mDbHelper;

    @Override
    protected void init(View mView) {
        mDbHelper = new DBHelper(mActivityContext);
        mRecyclerInstaImages = mView.findViewById(R.id.recycler_insta_images);
        mAdapter = new HomeInstagramImagesAdapter(mActivityContext, dataArrayList);
        fetchImagesFromDatabase();
    }

    private void fetchImagesFromDatabase() {
        Cursor mCursor = mDbHelper.getValues(DBHelper.TABLE_INSTA_IMAGES, null, null);

        if (mCursor != null
                && mCursor.getCount() != 0) {
            if (mCursor.moveToFirst()) {
                do {
                    String imageId = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_ID));
                    String imageLikes = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LIKES));
                    String imageLocation = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOCATION));
                    String imageLocationName = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOCATION_NAME));
                    String imageLowUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_LOW_RES_URL));
                    String imageStandardUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_STANDARD_RES_URL));
                    String imageThumbnailUrl = mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_INSTA_IMAGE_THUMBNAIL_URL));

                    InstagramImages mInstagramImages = new InstagramImages();
                    InstagramImages.Data mData = mInstagramImages.new Data();
                    InstagramImages.Data.Likes mLikes = mData.new Likes();
                    InstagramImages.Data.Images mImages = mData.new Images();
                    InstagramImages.Data.Images.Resolutions mLowRes = mImages.new Resolutions();
                    InstagramImages.Data.Images.Resolutions mStdRes = mImages.new Resolutions();
                    InstagramImages.Data.Images.Resolutions mThumbRes = mImages.new Resolutions();
                    InstagramImages.Data.Location mLocation = mData.new Location();

                    mData.setId(imageId);

                    mLocation.setLocation(imageLocation);
                    mLocation.setName(imageLocationName);
                    mData.setLocation(mLocation);

                    mLikes.setCount(Integer.valueOf(imageLikes));
                    mData.setLikes(mLikes);

                    mLowRes.setUrl(imageLowUrl);
                    mStdRes.setUrl(imageStandardUrl);
                    mThumbRes.setUrl(imageThumbnailUrl);
                    mImages.setThumbnail(mThumbRes);
                    mImages.setStandard_resolution(mStdRes);
                    mImages.setLow_resolution(mLowRes);
                    mData.setImages(mImages);

                    dataArrayList.add(mData);

                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }

        orientationBasedUI(getResources().getConfiguration().orientation);
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void orientationBasedUI(int orientation) {
        final WindowManager windowManager = (WindowManager) getActivity().getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE);
        final DisplayMetrics metrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(metrics);

        int columns = orientation == Configuration.ORIENTATION_PORTRAIT ? 3 : 5;

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), columns);
        mRecyclerInstaImages.setLayoutManager(layoutManager);

        if (mAdapter != null) {
            int size = metrics.widthPixels / columns;
            mAdapter.setImageSize(size);
        }
        mAdapter.updateData(dataArrayList);
        mRecyclerInstaImages.setAdapter(mAdapter);
    }
}
