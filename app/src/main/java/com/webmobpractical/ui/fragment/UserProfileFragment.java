package com.webmobpractical.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;

import com.webmobpractical.R;
import com.webmobpractical.ui.activity.BaseActivity;
import com.webmobpractical.ui.custom.CircleImageView;
import com.webmobpractical.utils.GlideUtils;

import java.util.HashMap;

import static com.webmobpractical.network.instagram.InstagramData.TAG_BIO;
import static com.webmobpractical.network.instagram.InstagramData.TAG_FOLLOWED_BY;
import static com.webmobpractical.network.instagram.InstagramData.TAG_FOLLOWS;
import static com.webmobpractical.network.instagram.InstagramData.TAG_FULL_NAME;
import static com.webmobpractical.network.instagram.InstagramData.TAG_MEDIA;
import static com.webmobpractical.network.instagram.InstagramData.TAG_PROFILE_PICTURE;
import static com.webmobpractical.network.instagram.InstagramData.TAG_USERNAME;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class UserProfileFragment extends BottomSheetDialogFragment implements GlideUtils.OnImageLoadListener {

    HashMap<String, String> userInfo = new HashMap<>();
    private BottomSheetBehavior.BottomSheetCallback mBottomSheetCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userInfo =
                    (HashMap<String, String>) getArguments().getSerializable(BaseActivity.KEY_USER_INFO);
        }
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.layout_user_profile, null);
        dialog.setContentView(contentView);
        //Set the coordinator layout behavior
        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        //Set callback
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetCallback);
        }

        TextView mTextUserFullName = contentView.findViewById(R.id.text_uesr_full_name);
        TextView mTextFollowerCount = contentView.findViewById(R.id.text_user_follower_count);
        TextView mTextMediaCount = contentView.findViewById(R.id.text_user_media_count);
        TextView mTextFollowingCount = contentView.findViewById(R.id.text_user_following_count);
        TextView mTextUserName = contentView.findViewById(R.id.text_user_name);
        TextView mTextUserBio = contentView.findViewById(R.id.text_user_bio);
        CircleImageView mUserImage = contentView.findViewById(R.id.image_user);

        GlideUtils.getImageFromPath(getActivity(), mUserImage, userInfo.get(TAG_PROFILE_PICTURE),
                this, 100, 100);
        mTextUserFullName.setText(userInfo.get(TAG_FULL_NAME));
        mTextMediaCount.setText(userInfo.get(TAG_MEDIA));
        mTextFollowerCount.setText(userInfo.get(TAG_FOLLOWED_BY));
        mTextFollowingCount.setText(userInfo.get(TAG_FOLLOWS));
        mTextUserName.setText(userInfo.get(TAG_USERNAME));
        mTextUserBio.setText(userInfo.get(TAG_BIO));
    }

    @Override
    public void onImageLoadingComplete() {

    }

    @Override
    public void onImageLoadError(String error) {

    }
}
