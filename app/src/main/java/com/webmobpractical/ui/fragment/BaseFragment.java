package com.webmobpractical.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public abstract class BaseFragment extends Fragment {

    protected FragmentManager fragmentManager;
    protected Context mActivityContext;
    private View view;

    protected abstract void init(View mView);

    public abstract int getLayoutId();

    public void orientationBasedUI(int orientation) {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getActivity().getSupportFragmentManager();
        mActivityContext = getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getLayoutId(), container, false);
        init(view);
        return view;
    }

    /**
     * Short toast.
     *
     * @param msg the msg
     */
    public void shortToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Long toast.
     *
     * @param msg the msg
     */
    public void longToast(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }
}
