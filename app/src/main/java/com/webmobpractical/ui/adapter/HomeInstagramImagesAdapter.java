package com.webmobpractical.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webmobpractical.R;
import com.webmobpractical.network.instagram.InstagramImages;
import com.webmobpractical.ui.custom.InstaImageView;

import java.util.ArrayList;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class HomeInstagramImagesAdapter extends RecyclerView.Adapter<HomeInstagramImagesAdapter.HomeInstagramImagesHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private int size;
    private int imageSize;
    private int padding;
    private ArrayList<InstagramImages.Data> mData;

    public HomeInstagramImagesAdapter(Context mActivityContext, ArrayList<InstagramImages.Data> dataArrayList) {
        this.mContext = mActivityContext;
        this.mData = dataArrayList;
        inflater = LayoutInflater.from(this.mContext);
        padding = this.mContext.getResources().getDimensionPixelSize(R.dimen.item_padding);
    }

    public void updateData(ArrayList<InstagramImages.Data> mData) {
        this.mData = mData;
    }


    @Override
    public HomeInstagramImagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.item_home_insta_images, parent, false);
        return new HomeInstagramImagesHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HomeInstagramImagesHolder viewHolder, int position) {

        InstagramImages.Data image = mData.get(position);

        viewHolder.mInstaImageView.getLayoutParams().width = imageSize;
        viewHolder.mInstaImageView.getLayoutParams().height = imageSize;

        viewHolder.itemView.getLayoutParams().width = size;
        viewHolder.itemView.getLayoutParams().height = size;

        viewHolder.mInstaImageView.setImageSize(size, size);

        viewHolder.view.setAlpha(1.0f);
        viewHolder.mInstaImageView.setPath(image.getImages().getStandard_resolution().getUrl());
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setImageSize(int size) {
        this.size = size;
        imageSize = size - padding * 2;
    }

    public class HomeInstagramImagesHolder extends RecyclerView.ViewHolder {
        private InstaImageView mInstaImageView;
        private View view;

        public HomeInstagramImagesHolder(View itemView) {
            super(itemView);
            mInstaImageView = itemView.findViewById(R.id.item_custom_insta_image_view);
            view = itemView.findViewById(R.id.view_alpha);
        }
    }
}
