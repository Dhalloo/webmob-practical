package com.webmobpractical.ui.custom;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public interface ActionBarView {

    void setTitle(String titleKey);

    void setRefreshVisibility(boolean visibility);

    void showUserProfile(String profileImage);
}