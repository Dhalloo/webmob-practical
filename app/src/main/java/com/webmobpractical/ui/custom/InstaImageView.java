package com.webmobpractical.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.webmobpractical.R;
import com.webmobpractical.ui.fragment.HomeFragment;
import com.webmobpractical.utils.GlideUtils;
import com.webmobpractical.utils.Utils;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class InstaImageView extends FrameLayout implements GlideUtils.OnImageLoadListener {
    private Context mContext;
    private ImageView imageView;
    private ProgressBar mProgressBar;
    private int mWidth, mHeight;
    private boolean isLoadingFinished = false;

    public InstaImageView(Context context) {
        super(context);
        init(context);
    }

    public InstaImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public InstaImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(mContext);
    }

    private void init(Context context) {
        mContext = context;
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rootView = inflater.inflate(R.layout.compound_insta_imageview, this, true);
        imageView = rootView.findViewById(R.id.image);
        mProgressBar = rootView.findViewById(R.id.progress_bar);
    }

    public ImageView getImageView() {
        return imageView;
    }

    public ProgressBar getProgressBar() {
        return mProgressBar;
    }

    public void setImageSize(int width, int height) {
        this.mWidth = width;
        this.mHeight = height;
    }

    public void setPath(String path) {
        Utils.print(">>>" + path);
        if (null == path) {
            path = HomeFragment.EMPTY_IMAGE_PATH;
        }
        mProgressBar.setVisibility(
                (isLoadingFinished || path.startsWith(HomeFragment.EMPTY_IMAGE_PATH)) ? View.GONE
                        : View.VISIBLE);
        GlideUtils.getImageFromPath(mContext, imageView, path, this, mWidth, mHeight);
    }

    @Override
    public void onImageLoadingComplete() {
        mProgressBar.setVisibility(View.GONE);
        isLoadingFinished = true;
    }

    @Override
    public void onImageLoadError(String error) {
        mProgressBar.setVisibility(View.GONE);
        isLoadingFinished = false;
    }
}
