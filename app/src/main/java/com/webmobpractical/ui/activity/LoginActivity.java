package com.webmobpractical.ui.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;

import com.webmobpractical.R;
import com.webmobpractical.database.DBHelper;
import com.webmobpractical.network.instagram.InstagramData;
import com.webmobpractical.network.instagram.OAuthAuthenticationListener;
import com.webmobpractical.utils.Constants;

import java.util.HashMap;

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private AnimationDrawable mAnimationDrawable;

    private Button mBtnInstagramLogin;
    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramData.WHAT_FINALIZE) {
                userInfoHashmap = mInstagramData.getUserInfo();
                DBHelper mDbHelper = new DBHelper(LoginActivity.this);
                mDbHelper.storeProfile(userInfoHashmap);
                navigate();
            } else if (msg.what == InstagramData.WHAT_FINALIZE) {
                shortToast(getString(R.string.message_no_internet_message));
            }
            return false;
        }
    });

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    @Override
    public void init() {
        mBtnInstagramLogin = findViewById(R.id.btn_instagram_login);
        mBtnInstagramLogin.setOnClickListener(this);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.constraint_login).getBackground();
        mAnimationDrawable.setEnterFadeDuration(1000);
        mAnimationDrawable.setExitFadeDuration(1000);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAnimationDrawable.start();
    }

    private void navigate() {
        Intent home = new Intent(this, HomeActivity.class);
        home.putExtra(BaseActivity.KEY_USER_INFO, userInfoHashmap);
        startActivity(home);
        finish();
    }

    public void loginWithInstagram() {
        mInstagramData = new InstagramData(this,
                Constants.CLIENT_ID,
                Constants.CLIENT_SECRET,
                Constants.CALLBACK_URL);

        if (mInstagramData.hasAccessToken()) {
            mInstagramData.fetchUserName(handler);
        } else {
            mInstagramData.authorize();
        }

        mInstagramData.setListener(new OAuthAuthenticationListener() {
            @Override
            public void onSuccess() {
                mInstagramData.fetchUserName(handler);
            }

            @Override
            public void onFail(String error) {
                shortToast(error);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_instagram_login:
                loginWithInstagram();
                break;
        }
    }
}

