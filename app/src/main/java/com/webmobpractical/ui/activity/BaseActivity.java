package com.webmobpractical.ui.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.webmobpractical.R;
import com.webmobpractical.network.instagram.InstagramData;
import com.webmobpractical.ui.custom.ActionBarView;
import com.webmobpractical.utils.Constants;
import com.webmobpractical.utils.GlideUtils;
import com.webmobpractical.utils.PreferenceUtils;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public abstract class BaseActivity extends AppCompatActivity implements ActionBarView, GlideUtils.OnImageLoadListener {

    public static final String KEY_USER_INFO = "com.webmobpractical.userinfo";
    public PreferenceUtils mPreferenceUtils;
    public InstagramData mInstagramData;
    public Toolbar mToolbar;

    public abstract int getLayoutId();

    public abstract void init();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        mPreferenceUtils = new PreferenceUtils(this);
        mToolbar = findViewById(R.id.toolbar);
        initializeToolbar();
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initializeToolbar();
    }

    protected void initializeToolbar() {
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle("");
        }
    }

    @Override
    public void setTitle(String titleKey) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            TextView title = findViewById(R.id.txt_toolbar_title);
            if (title != null) {
                title.setText(titleKey);
            }
        }
    }

    @Override
    public void showUserProfile(String profileImage) {

    }

    @Override
    public void setRefreshVisibility(boolean visible) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
        }
    }

    /**
     * Fragment transaction.
     *
     * @param container        the container
     * @param transactionType  the transaction type
     * @param fragment         the fragment
     * @param isAddToBackStack the is add to back stack
     * @param tag              the tag
     */
    public void fragmentTransaction(int container, int transactionType,
                                    Fragment fragment, boolean isAddToBackStack, String tag) {
        FragmentTransaction trans = getSupportFragmentManager().beginTransaction();

        trans.setCustomAnimations(R.anim.push_down_in, R.anim.push_down_out, R.anim.push_up_in, R.anim.push_up_out);

        switch (transactionType) {
            case (Constants.ADD_FRAGMENT):
                trans.add(container, fragment, tag);
                if (isAddToBackStack)
                    trans.addToBackStack(tag);
                break;
            case (Constants.REPLACE_FRAGMENT):
                trans.replace(container, fragment, tag);
                if (isAddToBackStack)
                    trans.addToBackStack(tag);
                break;
            case (Constants.REMOVE_FRAGMENT):
                trans.remove(fragment);
                getSupportFragmentManager().popBackStack();
                break;
        }
        trans.commit();
    }

    /**
     * Short toast.
     *
     * @param msg the msg
     */
    protected void shortToast(String msg) {
        Toast.makeText(BaseActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Long toast.
     *
     * @param msg the msg
     */
    protected void longToast(String msg) {
        Toast.makeText(BaseActivity.this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    protected void showMessageDialogWithOkButton(final String title, final String message,
                                                 DialogInterface.OnClickListener listener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.ok, listener);
        builder.create().show();
    }

    @Override
    public void onImageLoadingComplete() {

    }

    @Override
    public void onImageLoadError(String error) {

    }
}
