package com.webmobpractical.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.webmobpractical.R;
import com.webmobpractical.database.DBHelper;
import com.webmobpractical.network.api.WebServices;
import com.webmobpractical.network.api.WebServicesCallBackRequest;
import com.webmobpractical.network.api.WebServicesClient;
import com.webmobpractical.network.instagram.InstagramData;
import com.webmobpractical.network.instagram.InstagramImages;
import com.webmobpractical.ui.custom.CircleImageView;
import com.webmobpractical.ui.fragment.HomeFragment;
import com.webmobpractical.ui.fragment.MapViewFragment;
import com.webmobpractical.ui.fragment.UserProfileFragment;
import com.webmobpractical.utils.CheckInternetConnection;
import com.webmobpractical.utils.Constants;
import com.webmobpractical.utils.GlideUtils;
import com.webmobpractical.utils.Utils;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {

    private HashMap<String, String> userInfoHashmap = new HashMap<String, String>();
    private FloatingActionButton mFabChangeView;
    private boolean isHomeFragmentVisible = false;
    private HomeFragment mHomeFragment;
    private MapViewFragment mapViewFragment;
    private ArrayList<InstagramImages.Data> dataArrayList;
    private DBHelper mDbHelper;

    @Override
    public int getLayoutId() {
        return R.layout.activity_home;
    }

    @Override
    public void init() {
        if (getIntent() != null && getIntent().getExtras() != null) {
            userInfoHashmap =
                    (HashMap<String, String>) getIntent().getSerializableExtra(BaseActivity.KEY_USER_INFO);
        } else {
            return;
        }

        mDbHelper = new DBHelper(this);
        mInstagramData = new InstagramData(this, Constants.CLIENT_ID, Constants.CLIENT_SECRET,
                Constants.CALLBACK_URL);

        mFabChangeView = findViewById(R.id.fab_change_view);

        initializeToolbar();

        if (CheckInternetConnection.isConnectionAvailable(this)) {
            fetchInstagramImages();
        } else {
            showHomeFragment();
        }

        mFabChangeView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (isHomeFragmentVisible) {
                    shortToast(getResources().getString(R.string.show_map_image));
                } else {
                    shortToast(getResources().getString(R.string.show_home_image));
                }
                return false;
            }
        });

        mFabChangeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isHomeFragmentVisible) {
                    showMapFragment();
                } else {
                    showHomeFragment();
                }
            }
        });
    }

    @Override
    public void initializeToolbar() {
        super.initializeToolbar();
        setTitle(mPreferenceUtils.getUsername());
        showUserProfile(mPreferenceUtils.getProfileImage());
    }

    @Override
    public void showUserProfile(String profileImage) {
        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            CircleImageView mImageUserProfile = findViewById(R.id.image_user_profile);
            if (mImageUserProfile != null) {
                TypedValue tv = new TypedValue();
                int actionBarHeight = (int) getResources().getDimension(R.dimen.actionbar_size);
                if (getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                    actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
                }
                GlideUtils.getImageFromPath(this, mImageUserProfile,
                        profileImage, this, actionBarHeight, actionBarHeight);
                mImageUserProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        showProfile();
                    }
                });
            }
        }
    }

    private void showProfile() {
        UserProfileFragment mUserProfileFragment = new UserProfileFragment();
        Bundle mBundle = new Bundle();
        mBundle.putSerializable(BaseActivity.KEY_USER_INFO, userInfoHashmap);
        mUserProfileFragment.setArguments(mBundle);
        mUserProfileFragment.show(getSupportFragmentManager(), HomeActivity.class.getName());
    }

    private void showHomeFragment() {
        mHomeFragment = new HomeFragment();
        (this).fragmentTransact(Constants.ADD_FRAGMENT, mHomeFragment, false);
        isHomeFragmentVisible = true;
        mFabChangeView.setImageResource(R.drawable.ic_map);
    }

    private void showMapFragment() {
        mapViewFragment = new MapViewFragment();
        (this).fragmentTransact(Constants.REPLACE_FRAGMENT, mapViewFragment, true);
        isHomeFragmentVisible = false;
        mFabChangeView.setImageResource(R.drawable.ic_grid);
    }

    public void fragmentTransact(int fragTransactionType, Fragment fragment, boolean shouldAddToBackStack) {
        fragmentTransaction(R.id.base_frame, fragTransactionType,
                fragment, shouldAddToBackStack, fragment.getClass().getSimpleName());
    }

    public void fetchInstagramImages() {
        WebServices instagramService = WebServicesClient.getClient().create(WebServices.class);
        Call<InstagramImages> call =
                instagramService.getAllImages(userInfoHashmap.get(InstagramData.TAG_ID), mInstagramData.getTOken());
        call.enqueue(new WebServicesCallBackRequest<InstagramImages>() {

            @Override
            public void onResponse(Response<InstagramImages> response) {
                dataArrayList = response.body().getData();

                for (InstagramImages.Data mImages : dataArrayList) {
                    mDbHelper.storeImage(mImages);
                }
                showHomeFragment();
            }

            @Override
            public void onError() {
                Utils.print("ERROR FETCHING IMAGES...");
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                if (CheckInternetConnection.isConnectionAvailable(this)) {
                    fetchInstagramImages();
                } else {
                    showMessageDialogWithOkButton(getString(R.string.message_no_internet_title),
                            getString(R.string.message_no_internet_message), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                }
                break;
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mapViewFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
