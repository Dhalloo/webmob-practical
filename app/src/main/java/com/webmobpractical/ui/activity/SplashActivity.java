package com.webmobpractical.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.webmobpractical.R;
import com.webmobpractical.database.DBHelper;
import com.webmobpractical.network.instagram.InstagramData;
import com.webmobpractical.utils.CheckInternetConnection;
import com.webmobpractical.utils.Constants;

import java.util.HashMap;

public class SplashActivity extends BaseActivity {

    private final int SPLASH_TIME_DELAY = 3000;
    private InstagramData mInstagramData;
    private HashMap<String, String> userInfoHashmap;
    private TextView textWelcome;
    private AnimationDrawable mAnimationDrawable;
    private DBHelper mDbHelper;

    private Handler handler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            if (msg.what == InstagramData.WHAT_FINALIZE) {
                userInfoHashmap = mInstagramData.getUserInfo();
            }
            navigate();
            return false;
        }
    });

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    @Override
    public void init() {

        mDbHelper = new DBHelper(this);

        textWelcome = (TextView) findViewById(R.id.text_welcome);

        mAnimationDrawable = (AnimationDrawable) findViewById(R.id.constraint_splash).getBackground();
        mAnimationDrawable.setEnterFadeDuration(1000);
        mAnimationDrawable.setExitFadeDuration(1000);

        mInstagramData = new InstagramData(this, Constants.CLIENT_ID, Constants.CLIENT_SECRET,
                Constants.CALLBACK_URL);


        if (mInstagramData.hasAccessToken()) {
            userInfoHashmap = getUserFromDataBase();
            textWelcome.setText("Welcome To WebMobTech " +
                    "\n Practical " +
                    "\n You Are Already Logged In " +
                    "\n As " +
                    "\n" + mInstagramData.getUserName());
            if (CheckInternetConnection.isConnectionAvailable(this)) {
                mInstagramData.fetchUserName(handler); //For Update Profile
            } else {
                navigate();
            }
        } else {
            textWelcome.setText("Welcome To WebMobTech " +
                    "\n Practical");
            navigate();
        }
    }

    private HashMap<String, String> getUserFromDataBase() {
        HashMap<String, String> user = new HashMap<>();

        Cursor mCursor = mDbHelper.getValues(DBHelper.TABLE_PROFILE, null, null);
        if (mCursor != null
                && mCursor.getCount() != 0) {
            if (mCursor.moveToFirst()) {
                do {
                    user.put(InstagramData.TAG_ID, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_ID)));
                    user.put(InstagramData.TAG_USERNAME, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_USER_NAME)));
                    user.put(InstagramData.TAG_FULL_NAME, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_FULL_NAME)));
                    user.put(InstagramData.TAG_PROFILE_PICTURE, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_PROFILE_PIC)));
                    user.put(InstagramData.TAG_BIO, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_BIO)));
                    user.put(InstagramData.TAG_WEBSITE, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_WEBSITE)));
                    user.put(InstagramData.TAG_MEDIA, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_MEDIA_COUNT)));
                    user.put(InstagramData.TAG_FOLLOWED_BY, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_FOLLOWERS)));
                    user.put(InstagramData.TAG_FOLLOWS, mCursor.getString(mCursor.getColumnIndex(mDbHelper.COLUMN_PROFILE_USER_FOLLOWING)));
                } while (mCursor.moveToNext());
            }
            mCursor.close();
        }
        return user;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mAnimationDrawable.start();
    }

    private void navigate() {
        if (CheckInternetConnection.isConnectionAvailable(this)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (mInstagramData.hasAccessToken()
                            && userInfoHashmap.size() > 0) {
                        Intent home = new Intent(SplashActivity.this, HomeActivity.class);
                        home.putExtra(BaseActivity.KEY_USER_INFO, userInfoHashmap);
                        startActivity(home);
                    } else {
                        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    }
                    finish();
                }
            }, SPLASH_TIME_DELAY);

        } else {
            showMessageDialogWithOkButton(getString(R.string.message_no_internet_title),
                    getString(R.string.message_no_internet_message), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (mInstagramData.hasAccessToken()
                                    && userInfoHashmap.size() > 0) {
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        Intent home = new Intent(SplashActivity.this, HomeActivity.class);
                                        home.putExtra(BaseActivity.KEY_USER_INFO, userInfoHashmap);
                                        startActivity(home);
                                        finish();
                                    }
                                }, SPLASH_TIME_DELAY);
                            }
                        }
                    });
        }
    }
}
