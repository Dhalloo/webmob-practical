package com.webmobpractical.app;

import android.app.Application;

import com.webmobpractical.R;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class WebMobPracticalApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato_Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
