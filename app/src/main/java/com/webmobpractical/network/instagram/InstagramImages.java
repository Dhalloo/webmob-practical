package com.webmobpractical.network.instagram;

import java.util.ArrayList;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class InstagramImages {
    ArrayList<Data> data;

    public InstagramImages() {

    }

    public InstagramImages(ArrayList<Data> data) {
        this.data = data;
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public class Data {
        String id;
        Images images;
        Location location;
        Likes likes;

        public Data() {

        }

        public Data(String id, Images images, Location location, Likes likes) {
            this.id = id;
            this.images = images;
            this.location = location;
            this.likes = likes;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public Likes getLikes() {
            return likes;
        }

        public void setLikes(Likes likes) {
            this.likes = likes;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Images getImages() {
            return images;
        }

        public void setImages(Images images) {
            this.images = images;
        }

        public class Images {

            Resolutions low_resolution, thumbnail, standard_resolution;

            public Images(Resolutions low_resolution, Resolutions thumbnail,
                          Resolutions standard_resolution) {
                this.low_resolution = low_resolution;
                this.thumbnail = thumbnail;
                this.standard_resolution = standard_resolution;
            }

            public Images() {

            }

            public void initThumbnail(String path, String w, String h) {
                thumbnail = new Resolutions(path, w, h);
            }

            public Resolutions getLow_resolution() {
                return low_resolution;
            }

            public void setLow_resolution(Resolutions low_resolution) {
                this.low_resolution = low_resolution;
            }

            public Resolutions getThumbnail() {
                return thumbnail;
            }

            public void setThumbnail(Resolutions thumbnail) {
                this.thumbnail = thumbnail;
            }

            public Resolutions getStandard_resolution() {
                return standard_resolution;
            }

            public void setStandard_resolution(Resolutions standard_resolution) {
                this.standard_resolution = standard_resolution;
            }

            public class Resolutions {
                String url, width, height;

                public Resolutions(String url, String width, String height) {
                    this.url = url;
                    this.width = width;
                    this.height = height;
                }

                public Resolutions() {

                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }

                public String getWidth() {
                    return width;
                }

                public void setWidth(String width) {
                    this.width = width;
                }

                public String getHeight() {
                    return height;
                }

                public void setHeight(String height) {
                    this.height = height;
                }
            }
        }

        public class Location {
            double latitude;
            double longitude;
            String name;
            String location;

            public Location() {

            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public Location(double latitude, double longitude, String name) {
                this.latitude = latitude;
                this.longitude = longitude;
                this.name = name;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

        public class Likes {
            public int count;

            public Likes(int count) {
                this.count = count;
            }

            public Likes() {

            }

            public int getCount() {
                return count;
            }

            public void setCount(int count) {
                this.count = count;
            }
        }
    }
}
