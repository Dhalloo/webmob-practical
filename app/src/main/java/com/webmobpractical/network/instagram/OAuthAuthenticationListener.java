package com.webmobpractical.network.instagram;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public interface OAuthAuthenticationListener {
    void onSuccess();

    void onFail(String error);
}
