package com.webmobpractical.network.instagram;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Dhalloo on 11/13/2017.
 */

public class MapLocations {

    LatLng mPosition;
    String mTitle;
    String userImage;

    public MapLocations(double lat, double lng, String title, String userImage) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        this.userImage = userImage;
    }

    public LatLng getmPosition() {
        return mPosition;
    }

    public void setmPosition(LatLng mPosition) {
        this.mPosition = mPosition;
    }

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }
}
