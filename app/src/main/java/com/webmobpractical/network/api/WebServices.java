package com.webmobpractical.network.api;

import com.webmobpractical.network.instagram.InstagramImages;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public interface WebServices {

    @GET("/v1/users/{id}/media/recent/")
    Call<InstagramImages> getAllImages(@Path("id") String id,
                                       @Query("access_token") String accessToken);
}
