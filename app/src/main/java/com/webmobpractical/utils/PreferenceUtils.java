package com.webmobpractical.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class PreferenceUtils {

    private static final String API_USERNAME = "username";
    private static final String API_ID = "id";
    private static final String API_NAME = "name";
    private static final String API_ACCESS_TOKEN = "access_token";
    private static final String API_USER_PROFILE_PIC = "profile_pic";
    public final String APP_PREF = "webmob_practical_session";
    private SharedPreferences sharedPref;
    private SharedPreferences.Editor editor;

    public PreferenceUtils(Context context) {
        sharedPref = context.getSharedPreferences(APP_PREF, Context.MODE_PRIVATE);
        editor = sharedPref.edit();
    }

    public void storeUserDetails(String accessToken, String id, String username, String name, String profileImage) {
        editor.putString(API_ID, id);
        editor.putString(API_NAME, name);
        editor.putString(API_ACCESS_TOKEN, accessToken);
        editor.putString(API_USERNAME, username);
        editor.putString(API_USER_PROFILE_PIC, profileImage);
        editor.commit();
    }

    public void resetUserDetails() {
        editor.putString(API_ID, null);
        editor.putString(API_NAME, null);
        editor.putString(API_ACCESS_TOKEN, null);
        editor.putString(API_USERNAME, null);
        editor.putString(API_USER_PROFILE_PIC, null);
        editor.commit();
    }

    public String getUsername() {
        return sharedPref.getString(API_USERNAME, null);
    }

    public String getId() {
        return sharedPref.getString(API_ID, null);
    }

    public String getName() {
        return sharedPref.getString(API_NAME, null);
    }

    public String getAccessToken() {
        return sharedPref.getString(API_ACCESS_TOKEN, null);
    }

    public String getProfileImage() {
        return sharedPref.getString(API_USER_PROFILE_PIC, null);
    }

}
