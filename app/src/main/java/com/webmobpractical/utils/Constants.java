package com.webmobpractical.utils;

/**
 * Created by Dhalloo on 11/12/2017.
 */

public class Constants {

    public static final String CLIENT_ID = "ac6673929cb04d639b5fde01b5c8505c";
    public static final String CLIENT_SECRET = "f0593f5cf31a4398b893978fbd455e19";
    public static final String CALLBACK_URL = "https://webmobtech.com/";

    public static final int ADD_FRAGMENT = 0;
    public static final int REPLACE_FRAGMENT = 1;
    public static final int REMOVE_FRAGMENT = 2;

}
