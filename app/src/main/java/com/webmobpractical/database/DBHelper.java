package com.webmobpractical.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.webmobpractical.network.instagram.InstagramData;
import com.webmobpractical.network.instagram.InstagramImages;
import com.webmobpractical.utils.Utils;

import java.util.HashMap;

/**
 * Created by Dhalloo on 11/13/2017.
 */

public class DBHelper {

    public static final String COLUMN_ID = "_id";

    public static final String TABLE_PROFILE = "profile";
    public static final String COLUMN_PROFILE_USER_ID = "user_id";
    public static final String COLUMN_PROFILE_USER_PROFILE_PIC = "profile_pic";
    public static final String COLUMN_PROFILE_USER_USER_NAME = "user_name";
    public static final String COLUMN_PROFILE_USER_FULL_NAME = "full_name";
    public static final String COLUMN_PROFILE_USER_BIO = "user_bio";
    public static final String COLUMN_PROFILE_USER_WEBSITE = "website";
    public static final String COLUMN_PROFILE_USER_MEDIA_COUNT = "media_counts";
    public static final String COLUMN_PROFILE_USER_FOLLOWERS = "followers";
    public static final String COLUMN_PROFILE_USER_FOLLOWING = "following";
    public static final String CREATE_PROFILE = "CREATE TABLE IF NOT EXISTS "
            + TABLE_PROFILE + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_PROFILE_USER_ID + " TEXT, " + COLUMN_PROFILE_USER_USER_NAME + " TEXT, "
            + COLUMN_PROFILE_USER_FULL_NAME + " TEXT, " + COLUMN_PROFILE_USER_PROFILE_PIC + " TEXT, "
            + COLUMN_PROFILE_USER_BIO + " TEXT, " + COLUMN_PROFILE_USER_WEBSITE + " TEXT, "
            + COLUMN_PROFILE_USER_MEDIA_COUNT + " TEXT, " + COLUMN_PROFILE_USER_FOLLOWERS + " TEXT, "
            + COLUMN_PROFILE_USER_FOLLOWING + " TEXT," + "UNIQUE ("
            + COLUMN_PROFILE_USER_ID + " )" + " ON CONFLICT REPLACE);";

    public static final String TABLE_INSTA_IMAGES = "images";
    public static final String COLUMN_INSTA_IMAGE_ID = "image_id";
    public static final String COLUMN_INSTA_IMAGE_LIKES = "likes";
    public static final String COLUMN_INSTA_IMAGE_LOCATION = "location";
    public static final String COLUMN_INSTA_IMAGE_LOCATION_NAME = "location_name";
    public static final String COLUMN_INSTA_IMAGE_THUMBNAIL_URL = "thumbnail_url";
    public static final String COLUMN_INSTA_IMAGE_LOW_RES_URL = "low_res_url";
    public static final String COLUMN_INSTA_IMAGE_STANDARD_RES_URL = "standard_res_url";

    public static final String CREATE_INSTA_IMAGES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_INSTA_IMAGES + " ( " + COLUMN_ID + " INTEGER PRIMARY KEY, "
            + COLUMN_INSTA_IMAGE_ID + " TEXT, " + COLUMN_INSTA_IMAGE_LIKES + " TEXT, "
            + COLUMN_INSTA_IMAGE_LOCATION + " TEXT, " + COLUMN_INSTA_IMAGE_LOCATION_NAME + " TEXT, "
            + COLUMN_INSTA_IMAGE_THUMBNAIL_URL + " TEXT, " + COLUMN_INSTA_IMAGE_LOW_RES_URL + " TEXT, "
            + COLUMN_INSTA_IMAGE_STANDARD_RES_URL + " TEXT," + "UNIQUE ("
            + COLUMN_INSTA_IMAGE_ID + " )" + "ON CONFLICT REPLACE);";


    public static SQLiteDatabase mDatabase;
    public static String DB_NAME = "webmobtech";
    public static int DB_VERSION = 1;
    private String TAG = DBHelper.class.getName();
    private DatabaseHelper mHelper;
    private Context mContext;

    public DBHelper(Context mContext) {
        this.mContext = mContext;
        mHelper = getInstance(mContext);
        mDatabase = mHelper.getWritableDatabase();
    }

    private synchronized DatabaseHelper getInstance(Context context) {
        if (mHelper == null)
            mHelper = new DatabaseHelper(mContext);

        return mHelper;
    }

    public Cursor getValues(String mTableName, String[] coloum, String where) {
        Cursor cv = null;
        try {
            cv = mDatabase.query(mTableName, coloum, where, null, null, null,
                    null);
        } catch (Exception e) {
            Utils.print(TAG + " VALUES " + e.toString());
        }
        return cv;
    }

    public void deleteTable(String mTableName) {
        try {
            mDatabase.delete(mTableName, null, null);
        } catch (Exception e) {
            Utils.print(TAG + " DELETE " + e);
        }
    }

    public long storeProfile(HashMap<String, String> mUserInfo) {
        long rowId = 0;
        ContentValues mContentValues = new ContentValues();

        try {
            mContentValues.put(COLUMN_PROFILE_USER_ID, mUserInfo.get(InstagramData.TAG_ID));
            mContentValues.put(COLUMN_PROFILE_USER_USER_NAME, mUserInfo.get(InstagramData.TAG_USERNAME));
            mContentValues.put(COLUMN_PROFILE_USER_FULL_NAME, mUserInfo.get(InstagramData.TAG_FULL_NAME));
            mContentValues.put(COLUMN_PROFILE_USER_PROFILE_PIC, mUserInfo.get(InstagramData.TAG_PROFILE_PICTURE));
            mContentValues.put(COLUMN_PROFILE_USER_BIO, mUserInfo.get(InstagramData.TAG_BIO));
            mContentValues.put(COLUMN_PROFILE_USER_WEBSITE, mUserInfo.get(InstagramData.TAG_WEBSITE));
            mContentValues.put(COLUMN_PROFILE_USER_MEDIA_COUNT, mUserInfo.get(InstagramData.TAG_MEDIA));
            mContentValues.put(COLUMN_PROFILE_USER_FOLLOWERS, mUserInfo.get(InstagramData.TAG_FOLLOWED_BY));
            mContentValues.put(COLUMN_PROFILE_USER_FOLLOWING, mUserInfo.get(InstagramData.TAG_FOLLOWS));

            rowId = mDatabase.insert(TABLE_PROFILE, null, mContentValues);
            Utils.print(TAG + " PROFILE INSERTED " + mContentValues.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowId;
    }

    public long storeImage(InstagramImages.Data mImages) {
        long rowId = 0;
        ContentValues mContentValues = new ContentValues();

        try {
            mContentValues.put(COLUMN_INSTA_IMAGE_ID, mImages.getId());
            mContentValues.put(COLUMN_INSTA_IMAGE_LIKES, mImages.getLikes().getCount());
            if (mImages.getLocation() == null) {
                mContentValues.put(COLUMN_INSTA_IMAGE_LOCATION, "");
                mContentValues.put(COLUMN_INSTA_IMAGE_LOCATION_NAME, "");
            } else {
                mContentValues.put(COLUMN_INSTA_IMAGE_LOCATION,
                        mImages.getLocation().getLatitude() + "," + mImages.getLocation().getLongitude());
                mContentValues.put(COLUMN_INSTA_IMAGE_LOCATION_NAME, mImages.getLocation().getName());
            }
            mContentValues.put(COLUMN_INSTA_IMAGE_THUMBNAIL_URL, mImages.getImages().getThumbnail().getUrl());
            mContentValues.put(COLUMN_INSTA_IMAGE_LOW_RES_URL, mImages.getImages().getLow_resolution().getUrl());
            mContentValues.put(COLUMN_INSTA_IMAGE_STANDARD_RES_URL, mImages.getImages().getStandard_resolution().getUrl());

            rowId = mDatabase.insert(TABLE_INSTA_IMAGES, null, mContentValues);
            Utils.print(TAG + " IMAGE INSERTED " + mContentValues.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rowId;
    }

    public static class DatabaseHelper extends SQLiteOpenHelper {

        private DatabaseHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub
            db.execSQL(CREATE_PROFILE);
            db.execSQL(CREATE_INSTA_IMAGES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }
    }

}
